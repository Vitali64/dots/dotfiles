Vitali64's `vimrc`
==================
__________________
<img src="vim.png">


## Summary :
- [Requirements](#)
- [Installation](#)
- [Troubleshot](#)
- [Some useful features](#)



# Requirements :

### OS : 
 - Linux [RECOMMENDED] 
 - Windows [UNTESTED] 
 - MacOS [Last time I tested, autocompletion was broken]

### Terminal : 
    
 - Linux : a terminal that supports true colors like Alacritty
 - MacOS : Alacritty (RECOMMENDED)
 - Windows : -


# Installation

Make a backup of your old Vimrc first.

Download the vimrc [here](https://gitlab.com/vitali64/dotfiles/-/raw/master/vim/rc.vim?inline=false)

#### For NVim/NeoVim users :
Save the VimRc into ~/.config/nvim/init.vim

#### For Vim users:
Save the VimRc into ~/.vimrc 

And save the [`coc-settings.json`](https://gitlab.com/vitali64/dotfiles/-/raw/master/vim/coc-settings.json?inline=false) file on `~/.vim/` for Vim users, for NVim/NeoVim users : `~/.config/nvim` 

#### Then launch Vim/NeoVim :

    $ vim
or

    $ nvim


#### enter this command into Vim/NeoVim :

    :call Installvimplugplsplspls()

#### It will install [plug.vim](https://github.com/junegunn/vim-plug) and plugins that comes with this VimRc

#### After that, run this command, still in Vim/NeoVim :

    :call InstallCocPlugs()

#### It will install [ConquerOfCompletion (COC)](https://github.com/neoclide/coc.nvim) extensions



#### After that, relaunch your editor, you will get a VSCode-like Vim and you're ready to CODE :)

# Troubleshot

## If autocompletion doesn't work :

1. run `:CocInfo`
2. You will see the problem preventing autocompletion to work, if it is still not working, create an issue here if you think that the problem is only for this VimRc, or else, on the [ConquerOfCompletion's GitHub Repository](https://github.com/neoclide/coc.nvim/)


## If you have wierd symbols like this :


<img src="vim111.png" width="100%">

### There is 1 fix :

  Installing a NerdFont-compatible font (RECOMMENDED)

#### Installing NerdFonts

Just install a [NerdFont-compatible](www.nerdfonts.com) font like [CaskaydaCove Nerd Font Mono/CaskaydaCode](https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/CascadiaCode.zip)

and set the font you installed as the default font on your Terminal

## Useful Commands :

If you don't know really how to use this vimrc, here are some useful commands that you should try :

`:PlugInstall` : Install plugins specified in the section `call plug#begin` and `call plug#end`

`:TagBar` : Show a bar on which you can see imports, created variables, functions, and more !

`:NERDTree` or `:NERDTree /path/to/directory` : Show a bar where you can acces directories and files (Just like in VSCode, Atom or PyCharm)

`:e /path/to/file` : edit a file

`:bnext` or `:bprevious` : navigate trough tabs

`:CocList marketplace` : open the marketplace


And, also, do not hesitate to see what is in the VimRC and to modify it ...
I will be happy to see that my VimRc will be useful for beginners and to migrate from an IDE to vim / neovim

# Thanks for flying Vim !

