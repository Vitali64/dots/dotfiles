" Le VimRC de Vitali64

set incsearch






" Installation de Plugins avec VimPlug
call plug#begin()
Plug 'tpope/vim-sensible'     " Sensible.vim
Plug 'joshdick/onedark.vim'     "Theme OneDark (Tiré de Atom)
Plug 'preservim/nerdtree'     "NERDTree (Pour l'explorateur de fichiers)
Plug 'jistr/vim-nerdtree-tabs'     "NERDTree Tabs (Pour NerdTree)
Plug 'scrooloose/syntastic'     "Syntastic (Pour les syntaxes)
Plug 'majutsushi/tagbar'     "TagBar (Pour afficher la liste de variables, etc ...)
Plug 'vim-scripts/indentpython.vim'    "IndentPython (Pour Python)
Plug 'lepture/vim-jinja'     "Vim Jinja (Pour JinJa)
Plug 'neoclide/coc.nvim', {'branch': 'release'}     "ConquerOfCompletion [COC] (pour rendre Vim aussi intellegent que VSCode)
Plug 'pangloss/vim-javascript'     "VIm JavaScript (Pour JS)
Plug 'ryanoasis/vim-devicons'     "Vim DevIcons (pour les icones super stylés)
Plug 'vim-airline/vim-airline'     "VimAirLine (pour la barre d'etat personalisée)
Plug 'vim-airline/vim-airline-themes'     "VimAirLine Themes (pour les themes de la barre d'etat)
" Plug 'edkolev/tmuxline.vim'     "TmuxLine (pour que le theme de TMUX soit égal au theme de Vim)
" Plug 'ap/vim-buftabline' "BufTabNine
Plug 'sheerun/vim-polyglot' " Poligot.VIm
Plug 'tpope/vim-fugitive' " Support de GitHub/GitLab pour Vim
Plug 'mhinz/vim-startify' " Pour avoir un menu au demmarage de vim :) par Mhinz
Plug 'junegunn/gv.vim' " Pour voir les changements sur gitlab/github
Plug 'thaerkh/vim-indentguides' " pour les lignes d indentations
Plug 'morhetz/gruvbox' "Theme GRUVBOX
Plug 'terryma/vim-multiple-cursors' "Multiple cursors
Plug 'tpope/vim-surround' "Pour que l edition en HTML devienne plus simple
Plug 'sainnhe/sonokai' " Theme SONOKai
Plug 'fidian/hexmode' "Hex
call plug#end()


filetype plugin indent on
syntax on

" Toujours afficher la barre d'etat
set laststatus=0

" Activer le mode 256 couleurs
set t_Co=256
set t_ut=

" Activer le nombre de lignes
set number
set numberwidth=6 " Taille de nombres de lignes

" Le type(Unix) et mode d'encodage(UTF-8)
set fileformat=unix
set encoding=utf-8
set fileencoding=utf-8

" Menu de demmarage

"let g:startify_custom_header = [
"\ ' HELLOOOOOOO    ',
"\ '',
"\ '',
"\ ]

" Executer/Compiler le code C++ avec g++ sans passer par le terminal :)

nmap <F2> :!clear && python -c 'print("Running ...")' && sleep 0.5s && clear && g++ -o /tmp/runccodepls % && cd /tmp/ && sudo chmod +x runccodepls && ./runcodepls && rm /tmp/runccodepls<CR>

nmap <F3> :!clear && python -c 'print("Compiling ...")' && sleep 0.5s && clear && gcc -o CompiledCode %  && ./CompiledCode && rm /tmp/runccodepls<CR>




" Edition sensée
set tabstop=4
set shiftwidth=4
set softtabstop=4
set colorcolumn=80
set expandtab
set viminfo='25,\"50,n~/.viminfo


" indent/unindent with tab/shift-tab
nmap <Tab> >>
imap <S-Tab> <Esc><<i
nmap <S-tab> <<

" support de la souris
set mouse=a

" Le Theme
syntax on
set cursorline
set termguicolors
"set termguicolors
colorscheme onedark
hi Normal guibg=NONE ctermbg=NONE
let g:kolor_underlined=1

" Support pour l'italique
let &t_ZH="\e[3m"
let &t_ZR="\e[23m"

filetype on
filetype plugin indent on

" Desactiver la barre d'etat de vim par defaut pour le remplacer par une
" barre d'etat personalisé
set noshowmode

" Mettre un joli theme a la barre d'etat et mettre le mode
" PoweRLine(Necessite un style de texte compatible, Desactivé par defaut)
let g:airline_theme='onedark'

let g:airline_powerline_fonts =0
let g:airline_statusline_ontop=1
filetype on
filetype plugin indent on

" Desactiver la barre d'etat de vim par defaut pour le remplacer par une
" barre d'etat personalisé
set noshowmode

" Les Onglets
let g:airline#extensions#tabline#enabled = 1 " Les onglets

" restorer la place du curseur quand on quitte un fichier, puis, on l'ouvre encore
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Git sur la barre d'etat
set statusline=%<%f\ %h%m%r%{FugitiveStatusline()}%=%-14.(%l,%c%V%)\ %P "HELLOOOO"

" hide statusbar

let s:hidden_all = 1
set shortmess=F
function! ToggleHiddenAll()
    if s:hidden_all  == 0
        let s:hidden_all = 1
        set noshowmode
        set noruler
        set laststatus=0
        set noshowcmd
    else
        let s:hidden_all = 0
        set ruler
        set laststatus=2
    endif
endfunction

nnoremap <S-h> :call ToggleHiddenAll()<CR>


" Syntastic (Pour verifier la syntaxe)
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

" Creer une fonction qui permet d'installer les extensions de ConquerOfCompletion (COC)

function InstallCocPlugs()
	echo "Installing ConquerOfCompletion Extensions ..."
	
	CocInstall coc-marketplace coc-explorer coc-python coc-vimlsp coc-utils coc-tsserver coc-terminal coc-template coc-snippets coc-sh coc-prettier coc-pairs coc-lit-html coc-json coc-html coc-eslint coc-css


endfunction

" Creer une fonction qui permet d'installer PlugVIM et ses extensions

function Installvimplugpls()
    echo "Installation de vimplug ..."
    !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    echo "Installation des Plugins"
    PlugInstall
endfunction


" Copier, couper et coller
vmap <C-c> "+y
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <ESC>"+pa

" Mettre automatiquement le mode copier/coller lorsque on copie/colle du texte
" source: https://coderwall.com/p/if9mda/automatically-set-paste-mode-in-vim-when-pasting-in-insert-mode
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

function! XTermPasteBegin()
    set pastetoggle=<Esc>[201~
    set paste
    return ""
endfunction
inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

" Executer NERDTree (Explorateur de fichiers) au demmarage (Desactivé par defaut)
function NERDTreeplsimnotv64()
    autocmd VimEnter * NERDTree
endfunction

" call NERDTreeplsimnotv64()


" Fonction pour mise a jour

function Updatemepls()
    echo "not finished"

endfunction

" Startify

let g:startify_padding_left = 10
let g:startify_padding_right = 10

set noshowmode  " to get rid of thing like --INSERT--
set noshowcmd  " to get rid of display of last command
set shortmess+=F  " to get rid of the file name displayed in the command line bar


   set noshowmode
   set noruler
   set laststatus=0
   set noshowcmd
   set cmdheight=1

